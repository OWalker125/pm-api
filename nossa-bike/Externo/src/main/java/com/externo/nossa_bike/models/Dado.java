package com.externo.nossa_bike.models;

import org.springframework.stereotype.Component;

@Component
public class Dado {

    private int valor;
    private String ciclista;

    public int getValor() {
        return valor;
    }

    public String getCiclista() {
        return ciclista;
    }
}
