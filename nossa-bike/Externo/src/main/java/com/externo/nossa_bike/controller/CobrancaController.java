package com.externo.nossa_bike.controller;

import com.externo.nossa_bike.models.Cobranca;
import com.externo.nossa_bike.models.Dado;
import com.externo.nossa_bike.service.CobrancaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CobrancaController {

    @Autowired
    private CobrancaService cobranca;

    @PostMapping("/cobranca")
    public Cobranca postCobranca(@RequestBody Dado obj) {
        return cobranca.realizarCobranca(obj);
    }

}
