package com.externo.nossa_bike.service;

import com.externo.nossa_bike.models.Email;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailService {
    @Autowired
    private final JavaMailSender enviaEmail;

    public EmailService(JavaMailSender enviaEmail) {
        this.enviaEmail = enviaEmail;
    }

    public void enviarEmail(Email email) {
        var mensagem = new SimpleMailMessage();
        String emailPara = email.getEmail();
        mensagem.setTo(emailPara);
        String emailTexto = email.getMensagem();
        mensagem.setText(emailTexto);
        enviaEmail.send(mensagem);
    }
}
