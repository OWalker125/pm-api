package com.externo.nossa_bike.service;

import com.externo.nossa_bike.models.Cobranca;
import com.externo.nossa_bike.models.Dado;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

@Service
public class CobrancaService {

    private Vector<Dado>  vetor = new Vector<Dado>(10);

    @Autowired
    private Cobranca cobranca;

    public Cobranca realizarCobranca(Dado dado) {
        Cobranca.Status statusPagamento;
        cobranca.setId(dado.getCiclista());

        cobranca.setValor(dado.getValor());
        cobranca.setCiclista(dado.getCiclista());

        LocalDateTime dataHoraSolicitacao = LocalDateTime.now();
        cobranca.setHoraSolicitacao(String.valueOf(dataHoraSolicitacao));

        TimeUnit.SECONDS.toSeconds(30);

        statusPagamento = Cobranca.Status.PAGA;
        cobranca.setStatus(statusPagamento.toString());

        LocalDateTime dataHoraFinalizacao = LocalDateTime.now();
        cobranca.setHoraFinalizacao(String.valueOf(dataHoraFinalizacao));

        if(statusPagamento.toString().equals("PAGA")) {
            return cobranca;
        } else {
            vetor.add(dado);
            return cobranca;
        }
    }

    public void percorrerFila() {
        for(int i = 0; i < vetor.size(); i++) {
            realizarCobranca(vetor.get(i));
        }
        TimeUnit.HOURS.toHours(12);
        percorrerFila();
    }
}

