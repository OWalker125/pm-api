package com.externo.nossa_bike.controller;

import com.externo.nossa_bike.models.Email;
import com.externo.nossa_bike.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class EmailController {

    @Autowired
    private EmailService emailService;

    @PostMapping("/enviarEmail")
    public String enviarEmail(@RequestBody Email email) {
        emailService.enviarEmail(email);
        return "sucesso";
    }
}
